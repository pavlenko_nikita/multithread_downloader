#pragma once
#include "abstract_thread.h"

#include <winhttp.h>
#include <string>

#include <memory>

namespace MTDL
{
#define WM_FINISHDOWNLOAD WM_APP + 1

    class HttpFileDownloader : public AbstractThread
    {
    public:
        HttpFileDownloader( HWND hwndProgressBar, HWND hwndLabel, const std::wstring& path );

        void setUrl( const std::wstring& urlNew );
        
    protected:
        std::wstring m_path;
        std::wstring m_url;
        HWND m_hwndProgressBar;
        HWND m_hwndLabel;

        virtual DWORD run() override;
        long getFileSize( HINTERNET hRequest );
        std::wstring getFileName( HINTERNET hRequest );
        std::wstring getFileNameFromUrl();
        void getUrlInformation( std::wstring& host, std::wstring& path );
        std::wstring downloadFile( HINTERNET hRequest );
        void deleteHttpFromUrl();
        void logAndUiInform( const std::wstring& text );
    };
}
