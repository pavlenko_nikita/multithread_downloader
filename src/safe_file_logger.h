#pragma once
#include <Windows.h>

namespace MTDL
{
    class SafeFileLogger
    {
    public:
        static SafeFileLogger& instance()
        {
            static SafeFileLogger theSingleInstance;
            return theSingleInstance;
        }

        void setFileName( LPCWSTR fileName );
        void writeLog( LPCWSTR str );
        void writeLogWithTime( LPCWSTR str );

    private:
        HANDLE m_hFile;
        LPCWSTR m_newLine;
        DWORD m_bytesNewLine;

        SafeFileLogger();
        ~SafeFileLogger();
        SafeFileLogger( const SafeFileLogger& root ) = delete;
        SafeFileLogger& operator=( const SafeFileLogger& ) = delete;
    };
}
