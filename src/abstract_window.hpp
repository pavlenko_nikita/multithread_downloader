#pragma once

#include <Windows.h>

namespace MTDL
{
	template <typename DerivedType>
	class AbstractWindow
	{
	public:
		virtual ~AbstractWindow() = default;

		static LRESULT WINAPI mainHandleMessage( HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam )
		{
			DerivedType* pThis = reinterpret_cast<DerivedType*>(GetWindowLongPtr( hwnd, GWLP_USERDATA ));
			
			if( pThis )
			{
				return pThis->handleMessage( hwnd, uMsg, wParam, lParam );
			}
			else if( uMsg == WM_NCCREATE )
			{
				pThis = reinterpret_cast<DerivedType*>(reinterpret_cast<CREATESTRUCT*>(lParam)->lpCreateParams);
				SetWindowLongPtr( hwnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(pThis) );
				pThis->m_hwnd = hwnd;
			}	
			return DefWindowProc( hwnd, uMsg, wParam, lParam );
		}

		bool isCreated()
		{
			return m_hwnd ? true : false;
		}

		virtual bool create( LPCWSTR lpClassName,
							 LPCWSTR lpWindowName = L"",
							 DWORD dwStyle = 0,
							 HWND hWndParent = NULL,
							 HMENU hMenu = NULL,
							 int x = CW_USEDEFAULT,
							 int y = CW_USEDEFAULT,
							 int width = CW_USEDEFAULT,
							 int height = CW_USEDEFAULT,
							 HINSTANCE hInstance = NULL,
							 LPVOID lpParam = 0 ) = 0;

		inline HWND getHwnd() const
		{
			return m_hwnd;
		}

		void move( int x, int y, BOOL bRepaint = true )
		{
			RECT controlRect = getRect();
			MoveWindow( m_hwnd,
						x,
						y,
						controlRect.right - controlRect.left,
						controlRect.bottom - controlRect.top,
						bRepaint );
		}

		void resize( int width, int height, BOOL bRepaint = true )
		{
			RECT controlRect = getRect();

			MoveWindow( m_hwnd,
						controlRect.left,
						controlRect.top,
						width,
						height,
						bRepaint );
		}

		RECT getRect() const
		{
			RECT rc;
			GetWindowRect( m_hwnd, &rc );
			MapWindowPoints( HWND_DESKTOP, GetParent( m_hwnd ), reinterpret_cast<LPPOINT>(&rc), 2 );
			return rc;
		}

	protected:
		virtual LRESULT handleMessage( HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam )
		{
			return DefWindowProc( hwnd, uMsg, wParam, lParam );
		}

		HWND m_hwnd = NULL;
	};
}