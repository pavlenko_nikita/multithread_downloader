﻿#include "app_window.h"

#include <windowsx.h>
#include <CommCtrl.h>

#include <winhttp.h>
#include <string>

#include "safe_file_logger.h"

MTDL::AppWindow::AppWindow() :
	MainWindow(),
	m_fileDownloader( nullptr ),
	m_heightLabel( 20 )
{
	
}

LRESULT MTDL::AppWindow::handleMessage( HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam )
{
	switch( uMsg )
	{
		case WM_FINISHDOWNLOAD:
		{
			downloadNextFile( lParam );
			return 0;
		}
		HANDLE_MSG( m_hwnd, WM_CREATE, onCreate );
		HANDLE_MSG( m_hwnd, WM_SIZE, onSize );
		HANDLE_MSG( m_hwnd, WM_DESTROY, onDestroy );
		HANDLE_MSG( m_hwnd, WM_GETMINMAXINFO, onGetMinMaxInfo );
		default:
			return DefWindowProc( hwnd, uMsg, wParam, lParam );
	}
}

BOOL MTDL::AppWindow::onCreate( HWND hwnd, LPCREATESTRUCT lpCreateStruct )
{
	fillUrlQueue();
	//Init logger
	const int maxUserPath = 512;
	std::wstring folder( maxUserPath, '\0' );
	GetModuleFileName( GetModuleHandle( 0 ), &folder[0], maxUserPath );
	folder.erase( folder.find_last_of( '\\' ) + 1 );

	SafeFileLogger::instance().setFileName( std::wstring( folder + L"logger.txt" ).c_str() );
	SafeFileLogger::instance().writeLogWithTime( std::wstring( L"Start program" ).c_str() );

	//Find thread size
	UINT downloaderCount;
	SYSTEM_INFO sysinfo;
	GetSystemInfo( &sysinfo );

	if( m_queUrls.size() == 0 )
	{
		return TRUE;
	}
	else if( sysinfo.dwNumberOfProcessors > m_queUrls.size() )
	{
		downloaderCount = m_queUrls.size();
	}
	else
	{
		downloaderCount = sysinfo.dwNumberOfProcessors;
	}

	for( int i = 0; i < downloaderCount; ++i )
	{
		m_vecFileDownloaders.emplace_back( FileDownloadHandle() );
	}

	//Find and set label height
	TEXTMETRIC textMetrics = {};
	HDC hdc = GetDC( hwnd );
	GetTextMetrics( hdc, &textMetrics );
	m_heightLabel = textMetrics.tmHeight;
	ReleaseDC( hwnd, hdc );

	//Init controls
	createDownloadHandlerList( folder );

	runThreadPool();

	return TRUE;
}

void MTDL::AppWindow::onSize( HWND hwnd, UINT state, int cx, int cy )
{
	if( m_vecFileDownloaders.size() == 0 )
		return;

	int heightProgressBar = (cy - 40 - m_vecFileDownloaders.size() * (m_heightLabel + 3) ) / m_vecFileDownloaders.size();
	int widthControl = cx - 100;
	int heightCounter = 18;
	for( int i = 0; i < m_vecFileDownloaders.size(); ++i )
	{
		m_vecFileDownloaders[i].sFileName->resize( widthControl + 50, m_heightLabel );
		m_vecFileDownloaders[i].sFileName->move( 25, heightCounter + 2, true );
		heightCounter += m_heightLabel + 2;

		m_vecFileDownloaders[i].pbDownloading->resize( widthControl, heightProgressBar );
		m_vecFileDownloaders[i].pbDownloading->move( 50, heightCounter + 1, true );
		heightCounter += heightProgressBar + 1;
	}
}

void MTDL::AppWindow::fillUrlQueue()
{
	int nArgs = 0;
	LPWSTR* args = CommandLineToArgvW( GetCommandLineW(), &nArgs );

	for( int i = 1; i < nArgs; ++i )
	{
		m_queUrls.push( args[i] );
	}
}

void MTDL::AppWindow::runThreadPool()
{
	for( auto it = m_vecFileDownloaders.begin(); it != m_vecFileDownloaders.end(); ++it )
	{
		it->fileDownloader->start();
	}
}

void MTDL::AppWindow::downloadNextFile( LPARAM lParam )
{
	if( !m_queUrls.empty() )
	{
		HttpFileDownloader* pDownloader = reinterpret_cast<HttpFileDownloader*>(lParam);
		pDownloader->setUrl( m_queUrls.front() );
		m_queUrls.pop();
	}

}

void MTDL::AppWindow::createDownloadHandlerList( const std::wstring& folder)
{
	InitCommonControls();
	for( auto it = m_vecFileDownloaders.begin(); it != m_vecFileDownloaders.end(); ++it )
	{
		it->sFileName = std::make_unique<SimpleControl>();
		it->sFileName->create( L"STATIC",
							   L"",
							   WS_CHILD | WS_VISIBLE | SS_CENTER,
							   m_hwnd,
							   reinterpret_cast<HMENU>(0));
		it->pbDownloading = std::make_unique<SimpleControl>();
		it->pbDownloading->create( PROGRESS_CLASS,
								   L"",
								   WS_CHILD | WS_VISIBLE | PBS_SMOOTH,
								   m_hwnd,
								   reinterpret_cast<HMENU>(0));

		it->fileDownloader = std::make_unique<MTDL::HttpFileDownloader>( it->pbDownloading->getHwnd(), it->sFileName->getHwnd(), folder );
	}
}

void MTDL::AppWindow::onGetMinMaxInfo( HWND hwnd, LPMINMAXINFO lpMinMaxInfo )
{
	lpMinMaxInfo->ptMinTrackSize.x = 640;
	lpMinMaxInfo->ptMinTrackSize.y = 480;
}

void MTDL::AppWindow::onDestroy( HWND hwnd )
{
	SafeFileLogger::instance().writeLogWithTime( std::wstring( L"Close program" ).c_str() );
	PostQuitMessage( 0 );
}
