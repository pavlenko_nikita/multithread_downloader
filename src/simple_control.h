#pragma once

#include "abstract_window.hpp"

namespace MTDL
{
    class SimpleControl : public AbstractWindow<SimpleControl>
    {
    public:
        virtual bool create( LPCWSTR lpClassName,
                             LPCWSTR lpWindowName = L"",
                             DWORD dwStyle = 0,
                             HWND hWndParent = NULL,
                             HMENU hMenu = NULL,
                             int x = CW_USEDEFAULT,
                             int y = CW_USEDEFAULT,
                             int width = CW_USEDEFAULT,
                             int height = CW_USEDEFAULT,
                             HINSTANCE hInstance = NULL,
                             LPVOID lpParam = 0 ) override;
    };
}