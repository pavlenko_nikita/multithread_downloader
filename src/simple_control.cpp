#include "simple_control.h"

bool MTDL::SimpleControl::create( LPCWSTR lpClassName,
                                     LPCWSTR lpWindowName,
                                     DWORD dwStyle,
                                     HWND hWndParent,
                                     HMENU hMenu,
                                     int x,
                                     int y,
                                     int width,
                                     int height,
                                     HINSTANCE hInstance,
                                     LPVOID lpParam )
{
    m_hwnd = CreateWindow( lpClassName, lpWindowName, dwStyle, x, y, width, height, hWndParent, hMenu, hInstance, lpParam );
    if(!m_hwnd)
        return false;
    return true;
}
