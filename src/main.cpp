#include "app_window.h"

int WINAPI wWinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance, PWSTR pCmdLine, int nCmdShow )
{
	const UINT16 width = 800;
	const UINT16 height = 600;

	MTDL::AppWindow window;

	if( !window.create( L"MultithreadDownloader",
						L"Multithread Downloader",
						WS_OVERLAPPEDWINDOW | CS_VREDRAW | CS_HREDRAW,
						NULL, 
						0,
						GetSystemMetrics( SM_CXSCREEN ) / 2 - width / 2,
						GetSystemMetrics( SM_CYSCREEN ) / 2 - height / 2,
						width,
						height,
						hInstance ) )
	{
		return 0;
	}

	if( window.show( nCmdShow ) )
	{
		return 0;
	}

	window.run();

	return 0;
}