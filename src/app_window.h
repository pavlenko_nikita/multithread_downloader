#pragma once

#include "main_window.h"

#include <memory>
#include <queue>

#include "simple_control.h"
#include "http_file_downloader.h"

namespace MTDL
{
    class AppWindow : public MainWindow
    {
    public:
        AppWindow();

        virtual LRESULT handleMessage( HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam ) override;
    private:
        struct FileDownloadHandle
        {
            std::unique_ptr<MTDL::SimpleControl> sFileName = nullptr;
            std::unique_ptr<MTDL::SimpleControl> pbDownloading = nullptr;
            std::unique_ptr<MTDL::HttpFileDownloader> fileDownloader = nullptr;
        };

        std::unique_ptr<MTDL::HttpFileDownloader> m_fileDownloader;
        std::queue<std::wstring> m_queUrls;
        std::vector<FileDownloadHandle> m_vecFileDownloaders;
        int m_heightLabel;

        void fillUrlQueue();
        void runThreadPool();
        void downloadNextFile( LPARAM lParam );
        void createDownloadHandlerList( const std::wstring& folder);
                                                      
        //Msg handlers
        BOOL onCreate( HWND hwnd, LPCREATESTRUCT lpCreateStruct );
        void onSize( HWND hwnd, UINT state, int cx, int cy );
        void onGetMinMaxInfo( HWND hwnd, LPMINMAXINFO lpMinMaxInfo );
        void onDestroy( HWND hwnd );
    };
}