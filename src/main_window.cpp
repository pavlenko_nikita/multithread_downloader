#include "main_window.h"


bool MTDL::MainWindow::create( LPCWSTR lpClassName,
							   LPCWSTR lpWindowName,
							   DWORD dwStyle,
							   HWND hWndParent,
							   HMENU hMenu,
							   int x,
							   int y,
							   int width,
							   int height,
							   HINSTANCE hInstance,
							   LPVOID lpParam )
{
	WNDCLASS wc = { 0 };

	wc.lpfnWndProc = MainWindow::mainHandleMessage;
	wc.hInstance = hInstance;
	wc.lpszClassName = lpClassName;
	wc.hCursor = LoadCursor( NULL, IDC_ARROW );
	wc.hbrBackground = CreateSolidBrush( RGB( 128, 128, 128 ) );

	RegisterClass( &wc );

	m_hwnd = CreateWindow( lpClassName,
						   lpWindowName,
						   dwStyle,
						   x,
						   y,
						   width,
						   height,
						   hWndParent,
						   hMenu,
						   hInstance,
						   this );

	return isCreated();
}

BOOL MTDL::MainWindow::show( int nCmdShow )
{
	return ShowWindow( m_hwnd, nCmdShow );
}

void MTDL::MainWindow::run()
{
	MSG msg = { 0 };
	while( GetMessage( &msg, NULL, 0, 0 ) )
	{
		TranslateMessage( &msg );
		DispatchMessage( &msg );
	}
}

LRESULT MTDL::MainWindow::handleMessage( HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam )
{
	switch( uMsg )
	{
		case WM_DESTROY:
		{
			PostQuitMessage( 0 );
			return 0;
		}
	}
	return DefWindowProc( hwnd, uMsg, wParam, lParam );
}
