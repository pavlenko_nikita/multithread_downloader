#pragma once
#include <Windows.h>

namespace MTDL
{
	class AbstractThread
	{
	public:
		virtual ~AbstractThread() = default;

		void start();
	protected:
		DWORD m_idThread = 0;

		static DWORD threadProc( void* param );
		virtual DWORD run() = 0;
	};
}
