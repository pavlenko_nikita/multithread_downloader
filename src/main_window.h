#pragma once

#include "abstract_window.hpp"

namespace MTDL
{
    class MainWindow : public AbstractWindow<MainWindow>
    {
    public:
        virtual bool create( LPCWSTR lpClassName = L"MainWindow",
                             LPCWSTR lpWindowName = L"",
                             DWORD dwStyle = 0,
                             HWND hWndParent = NULL,
                             HMENU hMenu = NULL,
                             int x = CW_USEDEFAULT,
                             int y = CW_USEDEFAULT,
                             int width = CW_USEDEFAULT,
                             int height = CW_USEDEFAULT,
                             HINSTANCE hInstance = NULL,
                             LPVOID lpParam = 0 ) override;

        BOOL show( int nCmdShow );
        virtual void run();

        virtual LRESULT handleMessage( HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam ) override;
    };
}