#include "abstract_thread.h"

DWORD MTDL::AbstractThread::threadProc( void* param )
{
	AbstractThread* pThis = reinterpret_cast<AbstractThread*>(param);
	return pThis->run();
}

void MTDL::AbstractThread::start()
{
	CreateThread( NULL, 0, threadProc, reinterpret_cast<void*>(this), 0, &m_idThread );
}
