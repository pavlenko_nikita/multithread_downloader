#include "http_file_downloader.h"

#include <CommCtrl.h>
#include <winhttp.h>
#include "safe_file_logger.h"

MTDL::HttpFileDownloader::HttpFileDownloader( HWND hwndProgressBar, HWND hwndLabel, const std::wstring& path ) :
    AbstractThread(),
    m_path( path ),
    m_hwndProgressBar( hwndProgressBar ),
    m_hwndLabel( hwndLabel )
{

}

void MTDL::HttpFileDownloader::setUrl( const std::wstring& urlNew )
{
    m_url = urlNew;
}

DWORD MTDL::HttpFileDownloader::run()
{
    BOOL  bResults = FALSE;
    HINTERNET  hSession = NULL, hConnect = NULL, hRequest = NULL;
    std::wstring host = L"", path = L"", fileName = L"";

    hSession = WinHttpOpen( L"WinHTTP Example/1.0",
                            WINHTTP_ACCESS_TYPE_DEFAULT_PROXY,
                            WINHTTP_NO_PROXY_NAME,
                            WINHTTP_NO_PROXY_BYPASS,
                            0 );

    while( true )
    {
        if( hRequest ) WinHttpCloseHandle( hRequest );
        if( hConnect ) WinHttpCloseHandle( hConnect );
        m_url.clear();

        SendMessage( GetParent( m_hwndProgressBar ), WM_FINISHDOWNLOAD, NULL, reinterpret_cast<LPARAM>(this) );
        if( m_url.empty() )
            break;

        deleteHttpFromUrl();
        getUrlInformation( host, path );
        
        if( hSession )
            hConnect = WinHttpConnect( hSession, host.c_str(), INTERNET_DEFAULT_PORT, 0 );
        else
        {
            logAndUiInform( L"There was an error downloading file by url => " + m_url );
            break;
        }

        if( hConnect )
        {
            hRequest = WinHttpOpenRequest( hConnect,
                                           L"GET",
                                           path.c_str(),
                                           NULL,
                                           WINHTTP_NO_REFERER,
                                           WINHTTP_DEFAULT_ACCEPT_TYPES,
                                           WINHTTP_FLAG_SECURE );
        }
        else
        {
            logAndUiInform( L"There was an error connecting to url => " + m_url );
            continue;
        }

        if( hRequest )
        {
            std::wstring textWait = L"Wait ...";
            SendMessage( m_hwndLabel, WM_SETTEXT, 0, reinterpret_cast<LPARAM>(textWait.c_str()) );
            bResults = WinHttpSendRequest( hRequest,
                                           WINHTTP_NO_ADDITIONAL_HEADERS,
                                           NULL,
                                           WINHTTP_NO_REQUEST_DATA,
                                           0,
                                           WINHTTP_IGNORE_REQUEST_TOTAL_LENGTH,
                                           0 );
        }
        else
        {
            if( !path.empty() )
            {
                logAndUiInform( L"There was an error opening request to => " + path );
            }
            else
            {
                logAndUiInform( L"There was an error opening request to => " + m_url );
            }
            continue;
        }

        if( bResults )
        {
            bResults = WinHttpReceiveResponse( hRequest, NULL );
        }
        else
        {
            logAndUiInform( L"There was an error sending request to => " + host );
            continue;
        }

        if( bResults )
        {
            fileName = downloadFile( hRequest );
            if( fileName.empty() )
            {
                logAndUiInform( L"There was an error downloading file by url => " + m_url );
                continue;
            }
        }
        else
        {
            logAndUiInform( L"There was an error receiving response from => " + host );
            continue;
        }
    }

    if( hSession ) WinHttpCloseHandle( hSession );
    return 0;
}

long MTDL::HttpFileDownloader::getFileSize( HINTERNET hRequest )
{
    DWORD dwSize = 0;
    WinHttpQueryHeaders( hRequest,
                         WINHTTP_QUERY_CONTENT_LENGTH,
                         WINHTTP_HEADER_NAME_BY_INDEX,
                         NULL,
                         &dwSize,
                         WINHTTP_NO_HEADER_INDEX );

    if( GetLastError() == ERROR_INSUFFICIENT_BUFFER )
    {
        std::wstring buffer( dwSize / sizeof( WCHAR ), '\0' );

        WinHttpQueryHeaders( hRequest,
                             WINHTTP_QUERY_CONTENT_LENGTH,
                             WINHTTP_HEADER_NAME_BY_INDEX,
                             &buffer[0],
                             &dwSize,
                             WINHTTP_NO_HEADER_INDEX );

        long byteCount = std::stol( buffer );
        return byteCount;
    }
    return 0;
}

std::wstring MTDL::HttpFileDownloader::getFileName( HINTERNET hRequest )
{
    DWORD dwSize = 0;
    WinHttpQueryHeaders( hRequest,
                         WINHTTP_QUERY_CONTENT_DISPOSITION,
                         WINHTTP_HEADER_NAME_BY_INDEX,
                         NULL,
                         &dwSize,
                         WINHTTP_NO_HEADER_INDEX );

    if( GetLastError() == ERROR_INSUFFICIENT_BUFFER )
    {
        std::wstring buffer( dwSize / sizeof( WCHAR ), '\0' );

        WinHttpQueryHeaders( hRequest,
                             WINHTTP_QUERY_CONTENT_DISPOSITION,
                             WINHTTP_HEADER_NAME_BY_INDEX,
                             &buffer[0],
                             &dwSize,
                             WINHTTP_NO_HEADER_INDEX );

        size_t index = buffer.find( L"filename=\"" );
        if( index == std::wstring::npos )
            return {};

        index += 10;
        size_t endIndex = buffer.size() - 2 - index;
        std::wstring fileName = buffer.substr( index, endIndex );
        return fileName;
    }
    return {};
}

std::wstring MTDL::HttpFileDownloader::getFileNameFromUrl()
{
    size_t index = m_url.find_last_of( '/' );
    size_t endIndex = m_url.size() - index;

    std::wstring fileName = m_url.substr( index + 1, endIndex );
    return fileName;
}

void MTDL::HttpFileDownloader::getUrlInformation( std::wstring& host, std::wstring& path )
{
    if( m_url.empty() )
        return;

    size_t pos = m_url.find( L"//" );
    if( pos == std::wstring::npos )
    {
        pos = m_url.find_first_of( L"/", 0 );
        if( pos == std::wstring::npos )
            return;
    }
    else
    {
        pos = m_url.find_first_of( L"/", pos + 2 );
    }

    host = m_url.substr( 0, pos );
    path = m_url.substr( pos );
}

std::wstring MTDL::HttpFileDownloader::downloadFile( HINTERNET hRequest )
{
    ULONG totalBytesDownloaded = 0;
    DWORD dwSize = 0;
    HANDLE hFile = NULL;
    LPBYTE pszOutBuffer = NULL;
    LONG fileSize = 0;
    std::wstring fileName = L"";

    fileSize = getFileSize( hRequest );
    SendMessage( m_hwndProgressBar, PBM_SETRANGE32, 0, fileSize );

    fileName = getFileNameFromUrl();

    if( fileName.empty() )
        return fileName;

    SafeFileLogger::instance().writeLog( std::wstring(L"Start downloading file => " + fileName).c_str() );

    SendMessage( m_hwndLabel, WM_SETTEXT, 0, reinterpret_cast<LPARAM>(fileName.c_str()) );

    hFile = CreateFile( std::wstring( m_path + fileName ).c_str(),
                        GENERIC_READ | GENERIC_WRITE,
                        FILE_SHARE_READ,
                        NULL,
                        OPEN_ALWAYS,
                        FILE_ATTRIBUTE_NORMAL,
                        NULL );
   
    SendMessage( m_hwndProgressBar, PBM_SETPOS, totalBytesDownloaded, 0 );
    do
    {
        dwSize = 0;
        if( !WinHttpQueryDataAvailable( hRequest, &dwSize ) )
        {
            fileName = L"";
            break;
        }

        if( fileSize == 0 && dwSize != 0 )
        {
            SendMessage( m_hwndProgressBar, PBM_SETRANGE32, 0, dwSize + totalBytesDownloaded );
        }

        pszOutBuffer = new byte[dwSize + 1];
        if( !pszOutBuffer )
        {
            dwSize = 0;
            fileName = L"";
            break;
        }
        else
        {
            ZeroMemory( pszOutBuffer, dwSize + 1 );
            if( !WinHttpReadData( hRequest, reinterpret_cast<LPVOID>(pszOutBuffer), dwSize, NULL ) )
            {
                fileName = L"";
                delete[] pszOutBuffer;
                break;
            }
            else
            {
                WriteFile( hFile, pszOutBuffer, dwSize, NULL, NULL );
                if( dwSize != 0 )
                {
                    totalBytesDownloaded += dwSize;
                    SendMessage( m_hwndProgressBar, PBM_SETPOS, totalBytesDownloaded, 0 );
                }
                else
                {
                    SafeFileLogger::instance().writeLog( std::wstring( L"Finish downloading file => " + fileName).c_str() );
                }
            }
            delete[] pszOutBuffer;
        }
    } while( dwSize > 0 );
    CloseHandle( hFile );
    return fileName;
}

void MTDL::HttpFileDownloader::deleteHttpFromUrl()
{
    if( m_url.empty() )
        return;
    
    if( m_url.find( L"https://" ) != std::wstring::npos )
    {
        m_url.erase( 0, 8 );
        
    }
    else if( m_url.find( L"http://" ) != std::wstring::npos )
    {
        m_url.erase( 0, 7 );
    }
}

void MTDL::HttpFileDownloader::logAndUiInform( const std::wstring& text )
{
    SendMessage( m_hwndLabel, WM_SETTEXT, 0, reinterpret_cast<LPARAM>(text.c_str()) );
    SafeFileLogger::instance().writeLog( text.c_str() );
}
