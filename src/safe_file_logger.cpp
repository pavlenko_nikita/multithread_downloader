#include "safe_file_logger.h"
#include <string>

MTDL::SafeFileLogger::SafeFileLogger( ) :
    m_hFile( NULL ),
    m_newLine( L"\r\n" ),
    m_bytesNewLine( sizeof( WCHAR )* wcslen( m_newLine ) )
{
}

MTDL::SafeFileLogger::~SafeFileLogger()
{
    if( m_hFile )
        CloseHandle( m_hFile );
}

void MTDL::SafeFileLogger::setFileName( LPCWSTR fileName )
{
    if( m_hFile )
        CloseHandle( m_hFile );

    m_hFile = CreateFile( fileName,
                          FILE_GENERIC_WRITE,
                          FILE_SHARE_READ,
                          NULL,
                          OPEN_ALWAYS,
                          FILE_ATTRIBUTE_NORMAL,
                          NULL );
    SetFilePointer( m_hFile, 0, NULL, FILE_END );
}

void MTDL::SafeFileLogger::writeLog( LPCWSTR str )
{
    DWORD bytesToWrite = wcslen( str ) * sizeof( TCHAR );

    WriteFile( m_hFile, str, bytesToWrite, NULL, NULL );
    WriteFile( m_hFile, m_newLine, m_bytesNewLine, NULL, NULL );
}

void MTDL::SafeFileLogger::writeLogWithTime( LPCWSTR str )
{
    std::wstring buffer( 21 + wcslen( str ), '\0' );
    SYSTEMTIME sysTyme = {};
    GetLocalTime( &sysTyme );

    swprintf_s( &buffer[0],
                buffer.size(),
                std::wstring( std::wstring( L"%04u/%02u/%02u %02u:%02u:%02u ") + str ).data(),
                sysTyme.wYear,
                sysTyme.wMonth,
                sysTyme.wDay,
                sysTyme.wHour,
                sysTyme.wMinute,
                sysTyme.wSecond );

    writeLog( buffer.c_str() );
}
